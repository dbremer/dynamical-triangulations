#include <stdio.h>
#include <vector>
#include <iostream>
#include <time.h>
#include <queue>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <algorithm>
#include <stack> 
#include <utility>
#include <chrono>
#include <unordered_map>

#include "./random.h"

#include "./planarmap.h"

#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <Spectra/SymEigsSolver.h>
#include <Spectra/MatOp/SparseSymMatProd.h>

using namespace planar;

struct EdgeData {
	int vertexId;
	int distance;
	int faceId;
	bool inBoundary;
	bool canPeel;
	bool spinUp;
	EdgeData(){};
};

typedef typename Map<EdgeData>::EdgeHandle EdgeHandle;

// perform edge flip in map (returns true if succesful)
bool flipEdge(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle adjEdge = edge->getAdjacent();
	if( adjEdge == edge->getNext() ||
		adjEdge == edge->getPrevious() )
	{
		// edge is a loop: makes no sense to flip
		return false;
	}

	map.reattachEdge(edge,edge->getPrevious());
	map.reattachEdge(adjEdge,adjEdge->getPrevious());

	//Fix the face numbers
	int id1 = edge->data().faceId;
	int id2 = adjEdge->data().faceId;

	edge->getNext()->data().faceId = id1;
	edge->getPrevious()->data().faceId = id1;

	adjEdge->getNext()->data().faceId = id2;
	adjEdge->getPrevious()->data().faceId = id2;

	return true;
}

// subdivide triangle to which edge belongs into three triangles 
// (with one new vertex in the center of triangle)
void splitTriangle(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle otheredge1 = edge->getNext();
	EdgeHandle otheredge2 = edge->getPrevious();
	map.insertFace(edge,3);
	map.insertEdge(otheredge2,otheredge1->getPrevious());
}

// Assign vertex ids to the edges in the map and return number of vertices.
int assignVertexIds(Map<EdgeData> & map)
{
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().vertexId = -1;
	}
	
	int nextId = 0;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( (*it)->data().vertexId == -1 )
		{
			EdgeHandle vertexEdge = *it;
			do {
				vertexEdge->data().vertexId = nextId;
				vertexEdge = vertexEdge->getAdjacent()->getNext(); //vertexEdge has same id as getadjacent->getnext!
			} while (vertexEdge != *it);
			nextId++;
		}
	}
	return nextId;
}

//Assign face ids to the edges in the map
int assignFaceIds(Map<EdgeData> & map)
{
	for (Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().faceId = -1;
	}

	int faceNum = -1;
	for(Map<EdgeData>::EdgeIterator it = map.begin(); it!= map.end(); it++)
	{
		EdgeHandle edge = (*it);
		if( edge->data().faceId == -1 )
		{
			faceNum++;
			while( edge->data().faceId == -1 )
			{
				edge->data().faceId = faceNum;
				edge = edge->getNext();
			}
		}
	}	
	return faceNum + 1;
}

// write map to file in simple space-separated format
bool exportMap(Map<EdgeData> & map, std::string outputfile)
{
	std::ofstream file(outputfile.c_str());
	
	if( !file ) return false;
	
	file << map.numHalfEdges() << "\n"; 
	
	file << map.getRoot()->getId() << "\n";
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		file << (*it)->getNext()->getId() << " " 
			 << (*it)->getAdjacent()->getId() << "\n";
	}
	
	return true;
}

// read map from file in simple space-separated format
bool importMap(std::string inputfile, Map<EdgeData> & map)
{
	std::ifstream file(inputfile.c_str());
	
	if( !file )
		return false;
	
	map.clearMap();
	
	int size, root;
	file >> size >> root;
	
	while( map.numHalfEdges() < size )
	{
		map.newEdge();
	}
	map.setRoot(map.getEdge(root));	
	
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		int next, adj;
		file >> next >> adj;
		EdgeHandle nextEdge = map.getEdge(next);
		(*it)->setNext( nextEdge );
		nextEdge->setPrevious( *it );
		(*it)->setAdjacent( map.getEdge(adj) );
	}
	return true;
}

template<typename RNG>
EdgeHandle RandomEdge(Map<EdgeData> & map, RNG & rng)
{
	return map.getEdge(uniform_int(rng,map.numHalfEdges()));
}

// Determine graph distance to the vertex corresponding to the starting
// point of origin. The result is saved in data().distance.
void assignGraphDistance(Map<EdgeData> & map, EdgeHandle origin)
{
	// initialize all distances to -1
	std::queue<EdgeHandle> queue;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().distance = -1;
	}

	// set distance to 0 on all edges sharing same starting point
	EdgeHandle edge = origin;
	do {
		edge->data().distance = 0;
		queue.push(edge);
		edge = edge->getRotateCCW();
	} while( edge != origin);
	
	// do breadth-first search
	while( !queue.empty() )
	{
		edge = queue.front();
		queue.pop();
		if( edge->getAdjacent()->data().distance == -1 )
		{
			// the end point of edge has not been visited yet, so
			// assign distance one greater to the corresponding vertex
			EdgeHandle nextEdge = edge->getAdjacent();
			do {
				nextEdge->data().distance = edge->data().distance + 1;
				queue.push(nextEdge);
				nextEdge = nextEdge->getRotateCCW();
			} while( nextEdge != edge->getAdjacent() );			
		}
	}
}


//-------------------------------------------------------------------------------------------
//Functions for building the cluster
//-------------------------------------------------------------------------------------------
void reverseFaces(EdgeHandle current)
{
	if (current == nullptr)
	{
		return;
	}

	if (current->data().distance == 1)
	{
		return; //Already done this one
	}

	//Reverse the edges
	EdgeHandle iteratorEdge = current; 
	do
	{
		EdgeHandle temp = iteratorEdge->getNext();
		iteratorEdge->setNext(iteratorEdge->getPrevious());
		iteratorEdge->setPrevious(temp);
		iteratorEdge->data().distance = 1;
		iteratorEdge = iteratorEdge->getNext();
	} while (iteratorEdge != current);
	
	//Run on the adjacent edges
	reverseFaces(current->getNext()->getAdjacent());
	reverseFaces(current->getPrevious()->getAdjacent());
}

//Flip a cluster with a boundary that is already in the right order
void flipClusterInOrder2(Map<EdgeData>& map, std::vector<EdgeHandle>& boundary, Xoshiro256PlusPlus& rng)
{
	int bSize = boundary.size();
	//std::cout << "Flipping cluster of size " << bSize << std::endl;
	//Print out the boundary
	//for (int i = 0; i < bSize; i++)
	//{
	//	std::cout << "V: " << boundary[i]->data().vertexId << ", A:" << boundary[i]->getAdjacent()->data().vertexId << std::endl;
	//}
	//std::cout << "--------------------" << std::endl;

	if (bSize <= 3)
	{
		return;
	}
	
	//First detach the cluster from the map
	std::vector<EdgeHandle> outerBoundary;
	for (int i = 0; i < bSize; i++)
	{
		outerBoundary.push_back(boundary[i]->getAdjacent());
		boundary[i]->getAdjacent()->setAdjacent(nullptr);
		boundary[i]->setAdjacent(nullptr);
	}
	
	//Mirror every edge in the cluster
	//distance keeps track of whether we reversed it already
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		(*it)->data().distance = 0;
	}
	reverseFaces(boundary[0]); 

	//Reattach the cluster to the outer boundary
	int axis = uniform_int(rng, bSize);
	for (int i = 0; i <= (double) bSize / 2.0; i++)
	{
		int swap1 = (axis +  i) % bSize;
		int swap2 = (axis - i) % bSize < 0 ? (axis - i) % bSize + bSize : (axis - i) % bSize;
		boundary[swap1]->setAdjacent(outerBoundary[swap2]);
		boundary[swap2]->setAdjacent(outerBoundary[swap1]);
		outerBoundary[swap1]->setAdjacent(boundary[swap2]);
		outerBoundary[swap2]->setAdjacent(boundary[swap1]);
	}

}


//Alternative flip for cluster (flipClusterInOrder2 does not work perfectly)
//This flip correctly assigns vertex ids
void flipClusterInOrder(Map<EdgeData>& map, std::vector<EdgeHandle>& boundary, Xoshiro256PlusPlus& rng)
{
	int bSize = boundary.size();

	if (bSize <= 3)
	{
		return;
	}

	//std::cout << "In flipcluster" << std::endl;

	std::vector<EdgeHandle> outerBoundary;
	for (int i = 0; i < bSize; i++)
	{
		outerBoundary.push_back(boundary[i]->getAdjacent());
	}

	int angle = uniform_int(rng, bSize - 1);
	angle++;

	//Rotate the cluster
	for (int i = 0; i < bSize; i++)
	{
		int swap = (i + angle) % bSize;
		boundary[swap]->setAdjacent(outerBoundary[i]);
		outerBoundary[i]->setAdjacent(boundary[swap]);
	}

	//Fix vertex ids
	for (int i = 0; i < bSize; i++)
	{
		EdgeHandle startEdge = outerBoundary[i]->getNext();
		EdgeHandle current = startEdge;
		do
		{
			//std::cout << current->getId() << std::endl;
			current->data().vertexId = startEdge->data().vertexId;
			current = current->getRotateCCW();
		} while (current != startEdge);
	}

	//std::cout << "Out of flipcluster" << std::endl;
}

bool peelEdge(Map<EdgeData>& map, std::vector<EdgeHandle>& boundary, std::stack<EdgeHandle>& boundaryStack, EdgeHandle edgeToPeel, EdgeHandle mark, int index)
{
	if (index <= -1)
	{
		for (int i = 0; i < boundary.size(); i++)
		{
			if (edgeToPeel == boundary[i])
			{
				index = i;
				break; 
			}
		}
	}
	if (index <= -1)
	{
		//Already removed this edge from the boundary, cannot peel anymore
		return true;
	}

	//std::cout << "Peeling: " << index << std::endl;
	//Peel the edge
	EdgeHandle peeled = edgeToPeel->getAdjacent();
	if (peeled == mark || peeled == mark->getAdjacent())
	{
		//Stop the exploration 
		boundary.clear();
		return false;
	}

	if (peeled == 0)
	{
		//Something went wrong
		std::cerr << "peeled is 0!" << std::endl;
		return false;
	}

	//Integers that keep track if peeled->next->adj and peeled->next->next->adj are in the boundary and where
	int nextAdj = -1;
	int prevAdj = -1;
		
	//Add edges adjacent to peeled to the boundary if necessary, remove currentEdge from the boundary.
	//If edges adjacent to the face of peeled are already in the boundary, remove them
	//When removing an edge, make sure to add it to contained
	//Can this be done quicker, without going through the entire boundary?
	for (int i = 0; i < boundary.size(); i++)
	{
		if (boundary[i] == peeled->getNext()->getAdjacent())
		{
			nextAdj = i;
			if (boundary[i] == mark || boundary[i] == mark->getAdjacent())
			{
				//Stop 
				boundary.clear();
				return false;
			}
		}
		else if (boundary[i] == peeled->getPrevious()->getAdjacent())
		{
			prevAdj = i;
			if (boundary[i] == mark || boundary[i] == mark->getAdjacent())
			{
				//Stop 
				boundary.clear();
				return false;
			}
		}
	}


	//Remove random_choice
	boundary.erase(boundary.begin() + index);

	//When both are in the boundary, remove all from boundary (we close a hole here)
	if (nextAdj >= 0 && prevAdj >= 0)
	{
		//Remove indices, nextAdj and nextNextAdj 
		//Order matters!
		nextAdj = (index > nextAdj ? nextAdj : nextAdj - 1);
		prevAdj = (index > prevAdj ? prevAdj : prevAdj - 1);

		//Remove nextAdj
		boundary.erase(boundary.begin() + nextAdj);
		prevAdj = (nextAdj > prevAdj ? prevAdj : prevAdj - 1);

		//Remove prevAdj
		boundary.erase(boundary.begin() + prevAdj);

		//Do not add anything to the boundary
		//std::cout << "Filled in a hole" << std::endl; 
	}
	else if (nextAdj >= 0)
	{
		nextAdj = (index > nextAdj ? nextAdj : nextAdj - 1);

		//Remove nextAdj
		boundary.erase(boundary.begin() + nextAdj);

		//Add previous to the boundary and stack
		boundary.push_back(peeled->getPrevious());
		boundaryStack.push(peeled->getPrevious());

		//std::cout << "Added one edge 1" << std::endl;
	}
	else if (prevAdj >= 0)
	{
		prevAdj = (index > prevAdj ? prevAdj : prevAdj - 1);

		//Remove prevAdj
		boundary.erase(boundary.begin() + prevAdj);

		//Add next to the boundary and stack
		boundary.push_back(peeled->getNext());
		boundaryStack.push(peeled->getNext());

		//std::cout << "Added one edge 2" << std::endl;
	}
	else
	{
		//Peel the edge normally
		//Add next and nextnext to the boundary and stack
		boundary.push_back(peeled->getNext());
		boundary.push_back(peeled->getPrevious());
		boundaryStack.push(peeled->getNext());
		boundaryStack.push(peeled->getPrevious());

		//std::cout << "Added two edges" << std::endl;
	}

	return true;
}

void setVertexDistance(EdgeHandle edge, int value)
{
	EdgeHandle current = edge;
	do
	{
		current->data().distance = value;
		current = current->getAdjacent()->getNext();
	} while (current != edge);
	
}

EdgeHandle findBoundary(Map<EdgeData>& map, EdgeHandle mark)
{
	std::queue<EdgeHandle> queue;

	EdgeHandle edge = mark;
	do {
		edge->data().distance = 0;
		queue.push(edge);
		edge = edge->getRotateCCW();
	} while( edge != mark);
	
	// do breadth-first search
	while( !queue.empty() )
	{
		edge = queue.front();
		queue.pop();

		if (edge->data().inBoundary)
		{
			//Found the boundary 
			return edge;
		}

		if( edge->getAdjacent()->data().distance == -1 )
		{
			// the end point of edge has not been visited yet, so
			// assign distance one greater to the corresponding vertex
			EdgeHandle nextEdge = edge->getAdjacent();
			do {
				nextEdge->data().distance = edge->data().distance + 1;
				queue.push(nextEdge);
				nextEdge = nextEdge->getRotateCCW();
			} while( nextEdge != edge->getAdjacent() );			
		}
	}

	return nullptr;		
}

void fillCluster(Map<EdgeData>& map, std::vector<EdgeHandle>& boundary, std::vector<EdgeHandle>& boundaryInOrder, EdgeHandle mark)
{
	if (boundary.size() < 3)
	{
		return;
	}

	//Find the boundary element closest to the boundary
	assignGraphDistance(map, mark);
	int lowestDist = map.numVertices();
	int lowestIndex = -1;
	for(int i = 0; i < boundary.size(); i++)
	{
		if (boundary[i]->data().distance < lowestDist)
		{
			lowestDist = boundary[i]->data().distance;
			lowestIndex = i;
		}
	}

	//Print boundary
	//for (int i = 0; i < boundary.size(); i++)
	//{
	//	std::cout << "V: " << boundary[i]->data().vertexId << ", A: " << boundary[i]->getAdjacent()->data().vertexId << std::endl; 
	//}
	//std::cout << "Closest to mark: " <<  "V: " << boundary[lowestIndex]->data().vertexId << ", A: " << boundary[lowestIndex]->getAdjacent()->data().vertexId << std::endl;

	//Start dfs from this edge
	//Distance is "dfs colour"
	for (int i = 0; i < boundary.size(); i++)
	{
		boundary[i]->data().distance = 0;
	}

	std::stack<EdgeHandle> dfsStack;
	EdgeHandle startEdge = boundary[lowestIndex];
	dfsStack.push(startEdge);
	setVertexDistance(startEdge, 1);

	std::vector<std::pair<EdgeHandle, EdgeHandle>> parents; //parents[i] = (a,b) means that a has parent b

	bool foundCycle = false;
	while(!dfsStack.empty() && !foundCycle)
	{
		EdgeHandle currentEdge = dfsStack.top();
		dfsStack.pop();
		
		//Add all adjacent edges
		for (int i = 0; i < boundary.size(); i++)
		{
			if (boundary[i]->data().vertexId == currentEdge->getAdjacent()->data().vertexId)
			{
				if (boundary[i]->data().distance == 0)
				{
					//std::cout << "Adding: V: " << boundary[i]->data().vertexId << ", A: " << boundary[i]->getAdjacent()->data().vertexId << std::endl; 
					//Add this edge
					dfsStack.push(boundary[i]);
					setVertexDistance(boundary[i], 1);
					parents.push_back({boundary[i], currentEdge});
				}
				else
				{
					//Found a backedge, test if this is the first edge
					if (boundary[i] == startEdge)
					{
						//std::cout << "Found the cycle!" << std::endl;
						foundCycle = true; 
						//We have found the required cycle
						boundaryInOrder.push_back(boundary[i]);
						do
						{
							boundaryInOrder.push_back(currentEdge);
							for (int i = 0; i < parents.size(); i++)
							{
								if (parents[i].first == currentEdge)
								{
									currentEdge = parents[i].second;
									break;
								}
							}
						} while (currentEdge != startEdge);
					}
				}
			}
		}
		setVertexDistance(currentEdge, 2);	
	}

}

void fillCluster2(Map<EdgeData>& map, std::vector<EdgeHandle>& boundaryInOrder, EdgeHandle mark)
{
	//Find the boundary element closest to the boundary
	EdgeHandle closest = findBoundary(map, mark);
	if (closest == nullptr)
	{
		return;
	}
	std::unordered_map<int, bool> explored;
	
	//Start dfs from this edge
	EdgeHandle edge = closest;
	do
	{
		explored[edge->data().vertexId] = true;
		edge = edge->getAdjacent()->getNext();
	} while (edge != closest);

	std::stack<EdgeHandle> dfsStack;
	EdgeHandle startEdge = closest;
	dfsStack.push(startEdge);

	std::vector<std::pair<EdgeHandle, EdgeHandle>> parents; //parents[i] = (a,b) means that a has parent b

	bool foundCycle = false;
	while(!dfsStack.empty() && !foundCycle)
	{
		EdgeHandle currentEdge = dfsStack.top();
		dfsStack.pop();
		edge = currentEdge->getAdjacent();
		do
		{
			if (edge->data().inBoundary)
			{
				if (explored.find(edge->data().vertexId) != explored.end())
				{
					//Edge has not been explored yet
					dfsStack.push(edge);
					explored[edge->data().vertexId] = true;
					parents.push_back({edge, currentEdge});
				}
				else
				{
					//Backedge!
					if (edge == startEdge)
					{
						foundCycle = true; 
						//We have found the required cycle
						boundaryInOrder.push_back(edge);
						do
						{
							boundaryInOrder.push_back(currentEdge);
							for (int i = 0; i < parents.size(); i++)
							{
								if (parents[i].first == currentEdge)
								{
									currentEdge = parents[i].second;
									break;
								}
							}
						} while (currentEdge != startEdge);
					}
				}
				
			}
			
			edge = edge->getAdjacent()->getNext();
		} while(edge != currentEdge->getAdjacent());
	}

}

//Builds cluster by trying to peel every edge in boundary with probability p
//Similar to Wolff algorithm
void buildCluster(Map<EdgeData>& map, std::vector<EdgeHandle>& boundary, std::vector<EdgeHandle>& boundaryInOrder, double probability, Xoshiro256PlusPlus& rng)
{
	std::stack<EdgeHandle> bStack;
	//Randomly select the first edge
	EdgeHandle firstEdge = RandomEdge(map, rng);
	EdgeHandle currentEdge = firstEdge;
	do
	{
		bStack.push(currentEdge);
		boundary.push_back(currentEdge);
		currentEdge = currentEdge->getNext();
	} while (currentEdge != firstEdge);

	int added = 0;

	//Find the furthest away element from the firstEdge, this is the marked element
	assignGraphDistance(map, firstEdge);
	int furthest = 0;
	EdgeHandle markedEdge;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().distance > furthest)
		{
			furthest = (*it)->data().distance;
			markedEdge = (*it);
		}
	}

	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		(*it)->data().distance = 0;
	}
	
	//For each face, decide if it needs to be peeled
	//If distance = 1, it has tried to be peeled already
	while(!bStack.empty())
	{
		EdgeHandle next = bStack.top();
		bStack.pop();
		
		if (next->data().distance == 1)
		{
			//Already tried to peel this edge
			continue;
		}

		next->data().distance = 1;

		double random_double = uniform_real(rng);
		if (random_double <= probability)
		{
			//Peel this edge with certain probability
			bool success = peelEdge(map, boundary, bStack, next, markedEdge, -1);
			if (!success)
			{
				//Stop exploration, delete cluster
				boundary.clear();
				boundaryInOrder.clear();
				return; 
			}
			added++;
		}
	}

	//std::cout << "Made it to end of build cluster 2" << std::endl;
	//std::cout << added << std::endl;
	fillCluster(map, boundary, boundaryInOrder, markedEdge);
}

//Attempt without boundary vector
//It is way faster than the other buildcluster!
bool peelEdgeTest(Map<EdgeData>& map, std::stack<EdgeHandle>& boundaryStack, EdgeHandle edgeToPeel, EdgeHandle mark)
{
	EdgeHandle peeled = edgeToPeel->getAdjacent();
	//std::cout << "Peeling edge: " << edgeToPeel->getId() << std::endl;
	if (peeled == mark || peeled == mark->getAdjacent())
	{
		//Stop the exploration
		while(!boundaryStack.empty())
		{
			boundaryStack.pop();
		}
		return false;
	}

	if (peeled == 0)
	{
		//Something went wrong
		std::cerr << "peeled is 0!" << std::endl;
		while(!boundaryStack.empty())
		{
			boundaryStack.pop();
		}
		return false;
	}

	//Remove edgeToPeel
	edgeToPeel->data().inBoundary = false;

	EdgeHandle nextAdj = peeled->getNext()->getAdjacent();
	EdgeHandle prevAdj = peeled->getPrevious()->getAdjacent();

	if (nextAdj == mark || prevAdj == mark)
	{
		//Stop building, we reached the mark
		while(!boundaryStack.empty())
		{
			boundaryStack.pop();
		}

		return false;
	}

	//If both nextadj and prevadj are in, we should remove them from the boundary
	if (nextAdj->data().inBoundary && prevAdj->data().inBoundary)
	{
		//Remove them from the boundary
		nextAdj->data().inBoundary = false;
		prevAdj->data().inBoundary = false;
	}
	else if (nextAdj->data().inBoundary)
	{
		//Remove nextadj
		nextAdj->data().inBoundary = false;

		//Add previous
		peeled->getPrevious()->data().inBoundary = true;
		peeled->getPrevious()->data().canPeel = true;
		boundaryStack.push(peeled->getPrevious());

	}
	else if (prevAdj->data().inBoundary)
	{
		//Remove prevadj
		prevAdj->data().inBoundary = false;

		//Add next
		peeled->getNext()->data().inBoundary = true;
		peeled->getNext()->data().canPeel = true;
		boundaryStack.push(peeled->getNext());
	}
	else
	{
		//Peel normally
		peeled->getNext()->data().inBoundary = true;
		peeled->getNext()->data().canPeel = true;
		peeled->getPrevious()->data().inBoundary = true;
		peeled->getPrevious()->data().canPeel = true;
		boundaryStack.push(peeled->getNext());
		boundaryStack.push(peeled->getPrevious());
	}

	return true;
}

void fillClusterTest(Map<EdgeData>& map, std::vector<EdgeHandle>& boundaryInOrder, EdgeHandle mark)
{
	//Find bSize
	int bSize = 0;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().inBoundary)
		{
			bSize++;
		}
	}

	if (bSize < 3)
	{
		return;
	}

	//Find the boundary element closest to the mark
	assignGraphDistance(map, mark);
	int lowestDist = map.numVertices();
	EdgeHandle startEdge;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().inBoundary && (*it)->data().distance < lowestDist)
		{
			lowestDist = (*it)->data().distance;
			startEdge = *it;
		}

		//Reset distance
		(*it)->data().distance = 0;
	}

	if (!startEdge)
	{
		return;
	}

	//Start DFS from this edge
	//Distance is used as "dfs colour"
	std::stack<EdgeHandle> dfsStack;
	dfsStack.push(startEdge);
	setVertexDistance(startEdge, 1);

	std::vector<std::pair<EdgeHandle, EdgeHandle>> parents;

	bool foundCycle = false;

	while(!dfsStack.empty() && !foundCycle)
	{
		EdgeHandle currentEdge = dfsStack.top();
		dfsStack.pop();

		//Add all adjacent edges that are in the boundary to the stack
		EdgeHandle temp = currentEdge->getAdjacent();
		do
		{
			if (temp->data().inBoundary)
			{
				if (temp->data().distance == 0)
				{
					//Add to the stack
					dfsStack.push(temp);
					setVertexDistance(temp, 1);
					parents.push_back({temp, currentEdge});
				}
				else
				{
					//Found a backedge, test if this is the first edge
					if (temp == startEdge)
					{
						foundCycle = true;
						boundaryInOrder.push_back(temp);
						do
						{
							boundaryInOrder.push_back(currentEdge);
							for (int i = 0; i < parents.size(); i++)
							{
								if (parents[i].first == currentEdge)
								{
									currentEdge = parents[i].second;
									break;
								}
							}
						} while (currentEdge != startEdge);

						break;
					}
				}
			}

			temp = temp->getPrevious()->getAdjacent();
		} while(temp != currentEdge->getAdjacent());

		setVertexDistance(currentEdge, 2);
	}
}

void buildClusterTest(Map<EdgeData>& map, std::vector<EdgeHandle>& boundaryInOrder, double probability, Xoshiro256PlusPlus& rng)
{
	std::stack<EdgeHandle> bStack;
	
	//Randomly select the first edge
	EdgeHandle firstEdge = RandomEdge(map, rng);
	
	//Find the furthest away element from the firstEdge, this is the marked element
	assignGraphDistance(map, firstEdge);
	int furthest = 0;
	EdgeHandle markedEdge;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().distance > furthest)
		{
			furthest = (*it)->data().distance;
			markedEdge = (*it);
		}

		(*it)->data().inBoundary = false;
		(*it)->data().canPeel = false; 
	}

	//Set distances in stack
	EdgeHandle currentEdge = firstEdge;
	do
	{
		bStack.push(currentEdge);
		currentEdge->data().inBoundary = true;
		currentEdge->data().canPeel = true;
		currentEdge = currentEdge->getNext();
	} while (currentEdge != firstEdge);

	//std::cout << "Stack size: " << bStack.size() << std::endl;
	//For each face in the stack, decide if it needs to be peeled
	//distance = 0 - edge is not in boundary, but can be added
	//distance = 1 - edge is in boundary, can be peeled
	//distance = 2 - edge is in boundary, cannot be peeled anymore
	while(!bStack.empty())
	{
		EdgeHandle next = bStack.top();
		bStack.pop();
		
		if (!next->data().canPeel || !next->data().inBoundary)
		{
			//Already tried to peel this edge, or edge is no longer in boundary
			continue;
		}

		next->data().canPeel = false;

		double random_double = uniform_real(rng);
		if (random_double <= probability)
		{
			//std::cout << "Will peel!" << std::endl;
			//Peel this edge with certain probability
			bool success = peelEdgeTest(map, bStack, next, markedEdge);
			if (!success)
			{
				//Stop exploration, delete cluster
				boundaryInOrder.clear();
				for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
				{
					(*it)->data().inBoundary = false;
				}
				return; 
			}
			
		}
	}

	//std::vector<EdgeHandle> boundary;
	//for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	//{
	//	if ((*it)->data().inBoundary)
	//	{
	//		boundary.push_back((*it));
	//	}
	//}
	//std::cout << "Eind" << std::endl;
	//std::cout << "size: " << boundary.size() << std::endl; 
	//fillCluster(map, boundary, boundaryInOrder, markedEdge);

	fillClusterTest(map, boundaryInOrder, markedEdge);
}

void buildClusterTest2(Map<EdgeData>& map, std::vector<EdgeHandle>& boundaryInOrder, double probability, Xoshiro256PlusPlus& rng)
{
	std::stack<EdgeHandle> bStack;
	
	//Randomly select the first edge
	EdgeHandle firstEdge = RandomEdge(map, rng);
	
	//Find the furthest away element from the firstEdge, this is the marked element
	assignGraphDistance(map, firstEdge);
	int furthest = 0;
	EdgeHandle markedEdge;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().distance > furthest)
		{
			furthest = (*it)->data().distance;
			markedEdge = (*it);
		}

		(*it)->data().inBoundary = false;
		(*it)->data().canPeel = false; 
	}

	//Set distances in stack
	EdgeHandle currentEdge = firstEdge;
	do
	{
		bStack.push(currentEdge);
		currentEdge->data().inBoundary = true;
		currentEdge->data().canPeel = true;
		currentEdge = currentEdge->getNext();
	} while (currentEdge != firstEdge);

	//std::cout << "Stack size: " << bStack.size() << std::endl;
	//For each face in the stack, decide if it needs to be peeled
	//distance = 0 - edge is not in boundary, but can be added
	//distance = 1 - edge is in boundary, can be peeled
	//distance = 2 - edge is in boundary, cannot be peeled anymore
	while(!bStack.empty())
	{
		EdgeHandle next = bStack.top();
		bStack.pop();
		
		if (!next->data().canPeel || !next->data().inBoundary)
		{
			//Already tried to peel this edge, or edge is no longer in boundary
			continue;
		}

		next->data().canPeel = false;

		double random_double = uniform_real(rng);
		if (random_double <= probability)
		{
			//std::cout << "Will peel!" << std::endl;
			//Peel this edge with certain probability
			bool success = peelEdgeTest(map, bStack, next, markedEdge);
			if (!success)
			{
				//Stop exploration, delete cluster
				boundaryInOrder.clear();
				for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
				{
					(*it)->data().inBoundary = false;
				}
				return; 
			}
			
		}
	}

	fillCluster2(map, boundaryInOrder, markedEdge);
}

//-------------------------------------------------------------------------------------------
//Functions for the Ising Model
//Spins are assigned to faces
//Flip spins BEFORE filling holes in the cluster!
//Otherwise, cluster will not be all parallel spins
//-------------------------------------------------------------------------------------------
void setSpin(EdgeHandle edge, bool spin)
{
	EdgeHandle temp = edge;
	do
	{
		temp->data().spinUp = spin;
		temp = temp->getNext();
	} while (temp != edge);
}

void flipSpinsHelper(EdgeHandle edge, bool startSpin)
{
	if (edge == nullptr || !edge)
	{
		return;
	}

	if (edge->data().spinUp != startSpin)
	{
		//Already had this one
		return;
	}

	//Flip spins
	setSpin(edge, !startSpin);

	//Run on adjacent edges
	flipSpinsHelper(edge->getNext()->getAdjacent(), startSpin);
	flipSpinsHelper(edge->getPrevious()->getAdjacent(), startSpin);
}

void flipClusterSpins(std::vector<EdgeHandle>& boundary)
{
	int bSize = boundary.size();
	//First detach the cluster from the map
	std::vector<EdgeHandle> outerBoundary;
	for (int i = 0; i < bSize; i++)
	{
		outerBoundary.push_back(boundary[i]->getAdjacent());
		boundary[i]->getAdjacent()->setAdjacent(nullptr);
		boundary[i]->setAdjacent(nullptr);
	}

	//Flip all spins in the cluster
	//std::cout << "Here A" << std::endl;
	flipSpinsHelper(boundary[0], boundary[0]->data().spinUp);

	//std::cout << "Here B" << std::endl;
	//Reattach the cluster to the outer boundary
	for (int i = 0; i < bSize; i++)
	{
		boundary[i]->setAdjacent(outerBoundary[i]);
		outerBoundary[i]->setAdjacent(boundary[i]);
	}
}

void buildClusterIsing(Map<EdgeData>& map, std::vector<EdgeHandle>& boundaryInOrder, double probability, Xoshiro256PlusPlus& rng)
{
	std::stack<EdgeHandle> bStack;
	
	//Randomly select the first edge
	EdgeHandle firstEdge = RandomEdge(map, rng);
	bool spin = firstEdge->data().spinUp;

	//Find the furthest away element from the firstEdge, this is the marked element
	assignGraphDistance(map, firstEdge);
	int furthest = 0;
	EdgeHandle markedEdge;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().distance > furthest)
		{
			furthest = (*it)->data().distance;
			markedEdge = (*it);
		}

		(*it)->data().inBoundary = false;
		(*it)->data().canPeel = false; 
	}

	//Set distances in stack
	EdgeHandle currentEdge = firstEdge;
	do
	{
		bStack.push(currentEdge);
		currentEdge->data().inBoundary = true;
		currentEdge->data().canPeel = true;
		currentEdge = currentEdge->getNext();
	} while (currentEdge != firstEdge);

	//std::cout << "Stack size: " << bStack.size() << std::endl;
	//For each face in the stack, decide if it needs to be peeled
	//distance = 0 - edge is not in boundary, but can be added
	//distance = 1 - edge is in boundary, can be peeled
	//distance = 2 - edge is in boundary, cannot be peeled anymore
	while(!bStack.empty())
	{
		EdgeHandle next = bStack.top();
		bStack.pop();
		
		if (!next->data().canPeel || !next->data().inBoundary)
		{
			//Already tried to peel this edge, or edge is no longer in boundary
			continue;
		}

		next->data().canPeel = false;

		double random_double = uniform_real(rng);
		if ((random_double <= probability) && (next->getAdjacent()->data().spinUp == spin))
		{
			//std::cout << "Will peel!" << std::endl;
			//Peel this edge
			bool success = peelEdgeTest(map, bStack, next, markedEdge);
			if (!success)
			{
				//Stop exploration, delete cluster
				boundaryInOrder.clear();
				for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
				{
					(*it)->data().inBoundary = false;
				}
				return; 
			}
			
		}
	}

	//Now flip the spins in the cluster
	std::vector<EdgeHandle> boundary;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().inBoundary)
		{
			boundary.push_back(*it);
		}
	}

	//std::cout << "Here!" << std::endl;
	flipClusterSpins(boundary);
	//std::cout << "Here?" << std::endl;

	//Now we can fill the holes in the cluster
	fillClusterTest(map, boundaryInOrder, markedEdge);
}

//-------------------------------------------------------------------------------------------
//Functions to calculate observables
//-------------------------------------------------------------------------------------------
unsigned int calcRadius(Map<EdgeData>& map, int numFaces)
{
	unsigned int* adjacencyMatrix = new unsigned int[numFaces * numFaces];

	//Fill matrix
	for (int i = 0; i < numFaces; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			adjacencyMatrix[i * numFaces + j] = numFaces;
			adjacencyMatrix[j * numFaces + i] = numFaces;

			if (i == j)
			{
				adjacencyMatrix[i * numFaces + j] = 0;
			}
		}
	}

	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		EdgeHandle current = (*it);
		EdgeHandle adjacent = current->getAdjacent();
		adjacencyMatrix[current->data().faceId * numFaces + adjacent->data().faceId] = 1;
	}

	

	for (int k = 0; k < numFaces; k++)
	{
		for (int i = 0; i < numFaces; i++)
		{
			//Matrix is symmetrical, so store necessary data in bottom left part, then copy later
			for (int j = i; j < numFaces; j++)
			{
				//Always read from bottom left
				unsigned int mij = adjacencyMatrix[j * numFaces + i];
				unsigned int mik = adjacencyMatrix[(i > k ) ? i * numFaces + k : k * numFaces + i];
				unsigned int mkj = adjacencyMatrix[(j > k ) ? j * numFaces + k : k * numFaces + j];

				unsigned int newDistance = ( mij < mik + mkj) ? mij : mik + mkj;
				
				//Write to top right
				adjacencyMatrix[i * numFaces + j] = newDistance;
			}
		}

		//Top right has been updated now
		//Fix bottom left before next iteration
		for (int i = 0; i < numFaces; i++)
		{
			for (int j = 0; j < i; j++)
			{
				adjacencyMatrix[i * numFaces + j] = adjacencyMatrix[j * numFaces + i];
			}
		}
	}
	

	//Now calculate radius
	unsigned int radius = numFaces;
	for (int i = 0; i < numFaces; i++)
	{
		unsigned int ecc = 0;
		for (int j = 0; j < numFaces; j++)
		{
			ecc = ecc > adjacencyMatrix[i * numFaces + j] ? ecc : adjacencyMatrix[i * numFaces + j];
		}

		radius = (radius < ecc) ? radius : ecc;
	}
	delete[] adjacencyMatrix;
	
	return radius;
}

int calcEdgeEccentricity(Map<EdgeData>& map, EdgeHandle edge)
{
	int ecc = 0;
	assignGraphDistance(map, edge);
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if ((*it)->data().distance > ecc)
		{
			ecc = (*it)->data().distance;
		}
	}

	return ecc;
}

//Find the diameter based on the algorithm in the paper
int iFUB(Map<EdgeData>& map, EdgeHandle edge, int startBound, int error)
{
	int i = calcEdgeEccentricity(map, edge);
	//std::cout << "i: " << i << std::endl;
	int lowerBound = (startBound > i) ? startBound : i;
	int upperBound = 2 * i;

	//assignGraphDistance(map, edge);
	//int calls = 1;

	while(upperBound - lowerBound > error)
	{
		//Find the edges with distance i from startEdge
		std::vector<EdgeHandle> fringe;
		for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
		{
			if ((*it)->data().distance == i)
			{
				fringe.push_back((*it));
			}
		}

		//Find greatest eccentricity of the edges in fringe
		int ecc = 0;
		for (int j = 0; j < fringe.size(); j++)
		{
			int tempEcc = calcEdgeEccentricity(map, fringe[j]);
			//calls++;
			if (tempEcc > ecc)
			{
				ecc = tempEcc;
			}
		}

		int max = (lowerBound > ecc) ? lowerBound : ecc;
		if (max > 2 * (i - 1))
		{
			//std::cout << "Calls to bfs: " << calls << std::endl;
			return max;
		}
		else
		{
			lowerBound = ecc;
			upperBound = 2 * (i - 1);
		}

		i--;
	}
	//std::cout << "Calls to bfs: " << calls << std::endl;
	return lowerBound;
}

//Calculation the smallest num eigenvalues of the graph laplacian on the map
std::vector<double> laplacianEigenvalues(Map<EdgeData> & map, int num)
{   
    std::vector<Eigen::Triplet<double> > laplacianrules;
	laplacianrules.reserve(2*map.numHalfEdges());
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		int id1 = (*it)->data().vertexId;
		int id2 = (*it)->getAdjacent()->data().vertexId;
		// add -1 to matrix element (id1,id2)
		laplacianrules.push_back( Eigen::Triplet<double>(id1,id2,-1.0) );
		// and add 1 to the diagonal element (id1,id1)
		laplacianrules.push_back( Eigen::Triplet<double>(id1,id1,1.0) );
	}
    
    Eigen::SparseMatrix<double> laplacian(map.numVertices(),map.numVertices());
    laplacian.setFromTriplets(laplacianrules.begin(),laplacianrules.end());

    // See: https://spectralib.org/quick-start.html
    
    // Construct matrix operation object using the wrapper class SparseSymMatProd
    Spectra::SparseSymMatProd<double> matrixOperation(laplacian);

    // Construct eigen solver object, requesting the smallest num eigenvalues
	int ncv = 2 * (num+1);
    Spectra::SymEigsSolver< double, Spectra::SMALLEST_MAGN, Spectra::SparseSymMatProd<double> > eigs(&matrixOperation, num+1, ncv);

    // Initialize and compute
    eigs.init();
    eigs.compute(10000, 1.0e-7);

    // Retrieve results
    
    if(eigs.info() == Spectra::SUCCESSFUL)
    {
        Eigen::VectorXd evalues = eigs.eigenvalues();
        std::vector<double> evs;
        for(int i=0;i<num;++i)
        {
            evs.push_back(evalues(num-i-1));
        }
        return evs;
    }
    return {};
}

//Calculate degree sequence (O(n)), is a local observable
int* calcDegreeSequence(Map<EdgeData>& map, int numVertices)
{
	int* degSequence = new int[numVertices];
	for (int i = 0; i < numVertices; i++)
	{
		degSequence[i] = 0;
	}

	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		degSequence[(*it)->data().vertexId]++;
	}

	return degSequence;
}

int calcDiameterHighDegree(Map<EdgeData>& map)
{
	int vertices = assignVertexIds(map);
	int	numFaces = assignFaceIds(map);

	//Select edge with highest degree as start edge
	int* degSequence = new int[vertices];
	for (int i = 0; i < vertices; i++)
	{
		degSequence[i] = 0;
	}

	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		degSequence[(*it)->data().vertexId]++;
	}

	EdgeHandle startEdge;
	int degree = 0;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		if (degSequence[(*it)->data().vertexId] >= degree)
		{
			degree = degSequence[(*it)->data().vertexId];
			startEdge = (*it);
		}
	}
	delete[] degSequence;

	//Lower bound
	int lBound = 0;
	int diameter = iFUB(map, startEdge, lBound, 0);

	return diameter;
}

void countSize(EdgeHandle edge, int& size)
{
	//Outside of boundary
	if (edge == nullptr)
	{
		return;
	}

	//Have we already counted this edge?
	if (edge->data().faceId == -1)
	{
		return;
	}

	size++;

	//mark face as counted
	EdgeHandle current = edge;
	do
	{
		current->data().faceId = -1;
		current = current->getNext();
	} while(current != edge);

	//Perform on adjacent faces
	countSize(edge->getNext()->getAdjacent(), size);
	countSize(edge->getPrevious()->getAdjacent(), size);

}

int countClusterSize(Map<EdgeData>& map, std::vector<EdgeHandle>& boundary)
{
	int numFaces = assignFaceIds(map);
	int bSize = boundary.size();

	if (bSize < 3)
	{
		return 0;
	}

	//Detach the cluster from the map
	std::vector<EdgeHandle> outerBoundary;
	for (int i = 0; i < bSize; i++)
	{
		outerBoundary.push_back(boundary[i]->getAdjacent());
		boundary[i]->getAdjacent()->setAdjacent(nullptr);
		boundary[i]->setAdjacent(nullptr);
	}

	int size = 0;
	countSize(boundary[0], size);

	//Reattach cluster to the map
	for (int i = 0; i < bSize; i++)
	{
		boundary[i]->setAdjacent(outerBoundary[i]);
		boundary[i]->getAdjacent()->setAdjacent(boundary[i]);
	}

	size = (size >= numFaces / 2) ? numFaces - size : size;
	return size;
}

int calcMagnetisation(Map<EdgeData>& map)
{
	int m = 0;
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		m += ((*it)->data().spinUp ? 1 : -1);
	}
	
	m = m / 3; //Each value was counted 3 times, as it counts halfedges not faces

	return m;
}

//-------------------------------------------------------------------------------------------
//Measure functions that perform the actual measurements
//-------------------------------------------------------------------------------------------
void burnInNaive(Map<EdgeData>& map, int numBurns, Xoshiro256PlusPlus& rng)
{
	for (int i = 0; i < numBurns; i++)
	{
		flipEdge(map, RandomEdge(map, rng));
	}
}

void burnInCluster(Map<EdgeData>& map, int numBurns, double probability, Xoshiro256PlusPlus& rng)
{
	for (int i = 0; i < numBurns; i++)
	{	
		std::vector<EdgeHandle> boundary;
		std::vector<EdgeHandle> boundaryInOrder;
		//buildCluster(map, boundary, boundaryInOrder, probability, rng);
		buildClusterTest(map, boundaryInOrder, probability, rng);
		flipClusterInOrder(map, boundaryInOrder, rng);
	}
}

void measureDiameter(Map<EdgeData>& map, int enumerations, int flips, int burns, double probability, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	if (!file) 
	{
		std::cerr << "No File!" << std::endl;
		return; 
	}

	std::string output;
	//Burn the first triangulations
	burnInCluster(map, burns, probability, rng);

	for (int i = 0; i < enumerations; i++)
	{
		//Calculate diameter and output to a file
		std::cout << "Measuring diameter: " << i << std::endl;
		int diameter = calcDiameterHighDegree(map);

		//file << i << "," << diameter << "\n";
		output += std::to_string(diameter) + "-";
		//Do 100 sweeps
		
		auto t1 = std::chrono::high_resolution_clock::now();
		std::cout << "Flipping" << std::endl;
		for (int j = 0; j < flips; j++)
		{
			std::vector<EdgeHandle> boundary;
			std::vector<EdgeHandle> boundaryInOrder;
			buildClusterTest(map, boundaryInOrder, probability, rng);
			flipClusterInOrder(map, boundaryInOrder, rng);
		}
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
		output += std::to_string(duration.count()) + "\n";

		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	//std::cout << "Hier" << std::endl;
	file << output; 
	file.close();
}

void measureDiameterNaive(Map<EdgeData>& map, int enumerations, int flips, int burns, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	if (!file) 
	{
		std::cerr << "No File!" << std::endl;
		return; 
	}

	std::string output;
	burnInNaive(map, burns, rng);

	for (int i = 0; i < enumerations; i++)
	{
		//Calculate diameter and output to a file
		std::cout << "Measuring diameter: " << i << std::endl;
		int diameter = calcDiameterHighDegree(map);
		
		//file << i << "," << diameter << "\n";
		output += std::to_string(diameter) + "-";
		
		std::cout << "Flipping" << std::endl;
		auto t1 = std::chrono::high_resolution_clock::now();
		for (int j = 0; j < flips; j++)
		{
			flipEdge(map, RandomEdge(map, rng));
		}
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
		output += std::to_string(duration.count()) + "\n";
		
		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	file << output; 
	file.close();

}

/*
void measureRadius(Map<EdgeData>& map, int enumerations, int flips, double probability, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	if (!file) 
	{
		std::cerr << "No File!" << std::endl;
		return; 
	}
	
	int numFaces = assignFaceIds(map);
	std::string output;

	for (int i = 0; i < enumerations; i++)
	{
		//Calculate diameter and output to a file
		assignVertexIds(map);
		int radius = calcRadius(map, numFaces);
		output += std::to_string(radius) + "\n";
		for (int j = 0; j < flips; j++)
		{
			std::vector<EdgeHandle> boundary;
			std::vector<EdgeHandle> boundaryInOrder;
			buildClusterTest(map, boundaryInOrder, probability, rng);
			flipClusterInOrder(map, boundaryInOrder, rng);
		}
	}

	file << output; 
	file.close();
}

void measureRadiusNaive(Map<EdgeData>& map, int enumerations, int flips, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	if (!file) 
	{
		std::cerr << "No File!" << std::endl;
		return; 
	}
	
	int numFaces = assignFaceIds(map);
	std::string output;

	for (int i = 0; i < enumerations; i++)
	{
		//Calculate diameter and output to a file
		assignVertexIds(map);
		int radius = calcRadius(map, numFaces);
		//file << i << "," << diameter << "\n";
		output += std::to_string(radius) + "\n";
		for (int j = 0; j < flips; j++)
		{
			flipEdge(map, RandomEdge(map, rng));
		}
	}


	file << output; 
	file.close();
}
*/

void measureDegSeq(Map<EdgeData>& map, int enumerations, int flips, int burns, double probability, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	if (!file.is_open())
	{
		std::cerr << "Could not open file " << filename << std::endl;
		return;
	}

	int vertices = assignVertexIds(map);
	std::string output;
	burnInCluster(map, burns, probability, rng);

	for (int i = 0; i < enumerations; i++)
	{
		//Calculate deg seq and output to a file
		std::cout << "Measuring degree sequence: " << i << std::endl;
		int* degSeq = calcDegreeSequence(map, vertices);
		
		//Add degSeq to output
		for (int j = 0; j < vertices; j++)
		{
			output += std::to_string(degSeq[j]) + "-";
		}
		delete [] degSeq;
		
		auto t1 = std::chrono::high_resolution_clock::now();
		std::cout << "Flipping" << std::endl;
		
		
		for (int j = 0; j < flips; j++)
		{
			std::vector<EdgeHandle> boundary;
			std::vector<EdgeHandle> boundaryInOrder;
			//buildCluster(map, boundary, boundaryInOrder, probability, rng);
			buildClusterTest(map, boundaryInOrder, probability, rng);
			flipClusterInOrder(map, boundaryInOrder, rng);
		}

		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
		output += std::to_string(duration.count()) + "\n";
		
		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	file << output;
	file.close();
}

void measureDegSeqNaive(Map<EdgeData>& map, int enumerations, int flips, int burns, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	if (!file.is_open())
	{
		std::cerr << "Could not open file " << filename << std::endl;
		return;
	}

	assignVertexIds(map);
	std::string output;
	burnInNaive(map, burns, rng);

	for (int i = 0; i < enumerations; i++)
	{
		//Calculate deg seq and output to a file
		std::cout << "Measuring degree sequence: " << i << std::endl;
		int vertices = assignVertexIds(map);
		int* degSeq = calcDegreeSequence(map, vertices);
		
		//Add degSeq to output
		for (int j = 0; j < vertices; j++)
		{
			output += std::to_string(degSeq[j]) + "-";
		}
		delete [] degSeq;
		
		std::cout << "Flipping" << std::endl;

		auto t1 = std::chrono::high_resolution_clock::now();
		for (int j = 0; j < flips; j++)
		{
			flipEdge(map, RandomEdge(map, rng));
		}
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
		output += std::to_string(duration.count()) + "\n";

		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	file << output;
	file.close();
}

void measureLaplacian(Map<EdgeData>& map, int enumerations, int flips, int burns, double probability, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file; 
	file.open(filename);
	std::string output = ""; 

	if (!file) 
	{
		std::cerr << "No File!" << std::endl;
		return; 
	}

	assignVertexIds(map);
	burnInCluster(map, burns, probability, rng);

	for (int i = 0; i < enumerations; i++)
	{
		std::cout << "Measuring laplacian: " << i << std::endl; 
		std::vector<double> eigv = laplacianEigenvalues(map, 3);
		for (int j = 0; j < eigv.size(); j++)
		{
			output += std::to_string(eigv[j]) + "-";
		}

		std::cout << "Flipping" << std::endl;
		auto t1 = std::chrono::high_resolution_clock::now();
		for (int j = 0; j < enumerations; j++)
		{
			std::vector<EdgeHandle> boundary;
			std::vector<EdgeHandle> boundaryInOrder;
			buildClusterTest(map, boundaryInOrder, probability, rng);
			flipClusterInOrder(map, boundaryInOrder, rng);
		}

		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);

		output += std::to_string(duration.count()) + "\n";

		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	file << output; 
	file.close();
}

void measureLaplacianNaive(Map<EdgeData>& map, int enumerations, int flips, int burns, double probability, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file; 
	file.open(filename);
	std::string output; 

	if (!file) 
	{
		std::cerr << "No File!" << std::endl;
		return; 
	}

	
	burnInNaive(map, burns, rng);

	for (int i = 0; i < enumerations; i++)
	{
		std::cout << "Measuring laplacian: " << i << std::endl;
		std::vector<double> eigv = laplacianEigenvalues(map, 3);
		for (int j = 0; j < eigv.size(); j++)
		{
			output += std::to_string(eigv[j]) + "-";
		}

		std::cout << "Flipping" << std::endl;
		auto t1 = std::chrono::high_resolution_clock::now();
		for (int j = 0; j < enumerations; j++)
		{
			flipEdge(map, RandomEdge(map, rng));
		}

		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);

		output += std::to_string(duration.count()) + "\n";

		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	file << output; 
	file.close();
}

void measureClusterSize(Map<EdgeData>& map, int enumerations, double probability, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	std::string output;

	if (!file.is_open())
	{
		std::cerr << "Could not open file: " << filename << std::endl;
		return;
	}

	int numFaces = assignFaceIds(map);
	for (int i = 0; i < enumerations; i++)
	{
		//Calculate cluster size and output to a file
		assignVertexIds(map);
		numFaces = assignFaceIds(map);

		
		assignVertexIds(map);
		assignFaceIds(map);
		std::vector<EdgeHandle> boundary;
		std::vector<EdgeHandle> boundaryInOrder;
		//buildCluster(map, boundary, boundaryInOrder, probability, rng);
		buildClusterTest(map, boundaryInOrder, probability, rng);
		flipClusterInOrder(map, boundaryInOrder, rng);
		
		int cSize = countClusterSize(map, boundaryInOrder);
		output += std::to_string(cSize);
		output += "\n";
	}

	//std::cout << "Hier" << std::endl;
	file << output; 
	file.close();
}

void measureMagnetisation(Map<EdgeData>& map, int enumerations, int flips, int burns, double probability, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	std::string output;

	if (!file.is_open())
	{
		std::cerr << "Could not open file: " << filename << std::endl;
		return;
	}

	//Burn in 
	for (int i = 0; i < burns; i++)
	{	
		std::vector<EdgeHandle> boundary;
		std::vector<EdgeHandle> boundaryInOrder;
		//buildCluster(map, boundary, boundaryInOrder, probability, rng);
		buildClusterIsing(map, boundaryInOrder, probability, rng);
		flipClusterInOrder(map, boundaryInOrder, rng);
	}
	
	for (int i = 0; i < enumerations; i++)
	{
		//Measure
		std::cout << "Measuring value: " << i << std::endl;
		int magnetisation = calcMagnetisation(map);
		int diameter = calcDiameterHighDegree(map);

		//Add to output
		output += std::to_string(magnetisation) + "-" + std::to_string(diameter) + "-";

		//Perform a cluster flip
		auto t1 = std::chrono::high_resolution_clock::now();
		for (int j = 0; j < flips; j++)
		{
			std::vector<EdgeHandle> boundary;
			std::vector<EdgeHandle> boundaryInOrder;
			buildClusterIsing(map, boundaryInOrder, probability, rng);
			flipClusterInOrder(map, boundaryInOrder, rng);
		}

		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);

		output += std::to_string(duration.count()) + "\n";

		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	file << output;
	file.close();
}

void measureMagnetisationNaive(Map<EdgeData>& map, int enumerations, int flips, int burns, Xoshiro256PlusPlus& rng, std::string filename)
{
	std::ofstream file;
	file.open(filename);
	std::string output;

	if (!file.is_open())
	{
		std::cerr << "Could not open file: " << filename << std::endl;
		return;
	}

	//Burn in 
	for (int i = 0; i < burns; i++)
	{	
		flipEdge(map, RandomEdge(map, rng));
		EdgeHandle rand = RandomEdge(map, rng);
		setSpin(rand, !rand->data().spinUp);
	}
	
	for (int i = 0; i < enumerations; i++)
	{
		//Measure
		std::cout << "Measuring value: " << i << std::endl;
		int magnetisation = calcMagnetisation(map);
		int diameter = calcDiameterHighDegree(map);

		//Add to output
		output += std::to_string(magnetisation) + "-" + std::to_string(diameter) + "-";

		//Perform a cluster flip
		auto t1 = std::chrono::high_resolution_clock::now();
		for (int j = 0; j < flips; j++)
		{
			flipEdge(map, RandomEdge(map, rng));
			EdgeHandle rand = RandomEdge(map, rng);
			setSpin(rand, !rand->data().spinUp);
		}

		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);

		output += std::to_string(duration.count()) + "\n";

		if (i % 10 == 0)
		{
			file << output;
			output.clear();
		}
	}

	file << output;
	file.close();
}

//-------------------------------------------------------------------------------------------
//Main function
//-------------------------------------------------------------------------------------------
int main(int argc, char **argv)
{
	// initialize random number generator
	Xoshiro256PlusPlus rng(getseed());

	// initialize empty map with edges carrying the data of EdgeData
	Map<EdgeData> map;

	// make a simple triangulation consisting of 2 triangles glued along
	// their perimeter
	map.makePolygon(3);
	
	int numTriangles=0;
	while(argc == 0 &&  (numTriangles < 2 || numTriangles % 2 != 0) )
	{
		std::cout << "How many triangles? ";
		std::cin >> numTriangles;
	}

	std::string outputPath; 
	int mode = 0;
	int measurements = 0;
	int flips = 0;
	int burns = 0;
	double probability = 0;

	if (argc > 5)
	{
		//First argument is number of triangles
		std::stringstream value;
		value << argv[1];
		value >> numTriangles; 
		//std::cout << numTriangles << std::endl;
	
		//Second argument is number of measurements
		value.clear();
		value << argv[2];
		value >> measurements; 

		//Third argument is number of flips per measurement
		value.clear();
		value << argv[3];
		value >> flips; 

		//Fourth argument is number of burns
		value.clear();
		value << argv[4];
		value >> burns;

		//Fifth argument is probability
		value.clear();
		value << argv[5];
		value >> probability;
		std::cout << probability << std::endl;

		//Sixth argument is output file
		outputPath = argv[6];

		//Seventh argument is mode
		value.clear();
		value << argv[7];
		value >> mode;
		//std::cout << mode << std::endl;
		
	}
	else
	{
		return -1;
	}
	
	for(int n=2;n < numTriangles;n+=2)
	{
		// split random triangle (increasing number of triangles by 2)
		EdgeHandle randomEdge = RandomEdge(map,rng);
		splitTriangle(map,randomEdge);
	}
	
	int numHalfEdges = map.numHalfEdges();
	int numFaces = map.numFaces();
	std::cout << "#halfedges = " << numHalfEdges << ", #faces = " << numFaces << ", #vertices = " << map.numVertices() << "\n";

	assignVertexIds(map);
	assignFaceIds(map);

	//Start with parallel Ising state
	for (Map<EdgeData>::EdgeIterator it = map.begin(); it != map.end(); it++)
	{
		(*it)->data().spinUp = true;
	}
	
	switch(mode)
	{
		case 0:
			measureDiameter(map, measurements, flips, burns, probability, rng, outputPath);
			break;
		case 1:
			measureDegSeq(map, measurements, flips, burns, probability, rng, outputPath);
			break;
		case 2:
			//measureRadius(map, measurements, flips, probability, rng, outputPath);
			break;
		case 3:
			measureDiameterNaive(map, measurements, flips, burns, rng,  outputPath);
			break;
		case 4:
			//measureRadiusNaive(map, measurements, flips, rng, outputPath);
			break;
		case 5:
			measureDegSeqNaive(map, measurements, flips, burns, rng, outputPath);
			break;
		case 6:
			measureLaplacian(map, measurements, flips, burns, probability, rng, outputPath);
			break;
		case 7:
			measureClusterSize(map, measurements, probability, rng, outputPath);
			break;
		case 8:
			measureMagnetisation(map, measurements, flips, burns, probability, rng, outputPath);
			break;
		case 9:
			measureLaplacianNaive(map, measurements, flips, burns, probability, rng, outputPath);
			break;
		case 10:
			measureMagnetisationNaive(map, measurements, flips, burns, rng, outputPath);
		default:
			std::cerr << "No mode!" << std::endl;
			return -1; 
	}

	return 0;
}